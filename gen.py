from basf2 import *
from ROOT import Belle2
import sys

path = create_path()
setter = path.add_module("EventInfoSetter", evtNumList=[100000])
print_params(setter)

generator = path.add_module("EvtGenInput", userDECFile=sys.argv[1])
print_params(generator)

path.add_module('RootOutput', outputFileName=sys.argv[2])

process(path)
